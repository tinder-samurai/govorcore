# GovorCore [![codebeat badge](https://codebeat.co/badges/784389be-ef7d-472f-b9fc-218362ed4346)](https://codebeat.co/projects/github-com-henryco-govorcore-master)

#### Simple server / client chat app, based on java8, jfx2 and [struct](https://bitbucket.org/henryco/struct/src/master/).

* [**Download**](https://drive.google.com/open?id=0BzwCB78J-oVxaVZ1Q2VtaGVDQkE)
* [**Doc (PL)**](https://drive.google.com/open?id=0BzwCB78J-oVxVVk0aUxnZmZwaWs)

![](https://bitbucket.org/henryco/govorcore/raw/d1b764868e085008a1d96ac7a9cbed8c8f94f974/promo.png)




